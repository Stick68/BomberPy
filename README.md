# Projet 2024 - BomberPy

## Protocole d'utilisation
___

### Windows :
- #### Installation :
    - Installer python 3 ou supérieur ([ici](https://www.python.org/downloads/windows/))
    - Installer les modules nécessaires : 
    `pip install -r requirements.txt`
- #### Exécution :
    - Depuis le repertoire `sources`: `py main.py`

